//
//  ViewController.swift
//  PHS Think Pink
//
//  Created by Ben Grass on 9/2/15.
//  Copyright (c) 2015 Ben Grass. All rights reserved.
//

import UIKit
import CoreData

class ViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet var label: UILabel!
    @IBOutlet var submitButton: UIButton!
    @IBOutlet var nameField: UITextField!
    @IBOutlet var emailField: UITextField!
    @IBOutlet var pnField: UITextField!
    @IBOutlet var facebookCtrl: UISegmentedControl!
    @IBOutlet var ribbon: UIImageView!
    
    var data = [NSManagedObject]()
    
    let darkPink = UIColor(red: 171/255, green: 3/255, blue: 92/255, alpha: 1)
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func prefersStatusBarHidden() -> Bool {
        return true
    }
    
    override func viewDidLoad() {
        loadTableContents()
        super.viewDidLoad()
        ribbon.addGestureRecognizer(UILongPressGestureRecognizer(target: self, action: "askExport:"))
        label.addGestureRecognizer(UILongPressGestureRecognizer(target: self, action: "askClear:"))
        submitButton.addTarget(self, action: "submit", forControlEvents: UIControlEvents.TouchUpInside)
        submitButton.enabled = false
        submitButton.backgroundColor = UIColor.lightGrayColor()
        nameField.addTarget(self, action: "textFieldChanged", forControlEvents: UIControlEvents.EditingChanged)
        emailField.addTarget(self, action: "textFieldChanged", forControlEvents: UIControlEvents.EditingChanged)
        pnField.addTarget(self, action: "autoFormatTextField", forControlEvents: UIControlEvents.EditingChanged)
        nameField.delegate = self
        emailField.delegate = self
        pnField.delegate = self
        
        //label.font = UIFont(name: "Gotham", size: 44.0)
        
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    func loadTableContents() {
        
        let appDelegate =
        UIApplication.sharedApplication().delegate as! AppDelegate
        
        let managedContext = appDelegate.managedObjectContext!
        
        //2
        let fetchRequest = NSFetchRequest(entityName:"Member")
        //3
        
        var fetchedResults = []
        do {
            try fetchedResults = managedContext.executeFetchRequest(fetchRequest) as! [NSManagedObject]
        } catch _ as NSError {

        }
        
        data = fetchedResults as! [NSManagedObject]
        
        print(data.count)
    }

    
    func textFieldChanged() {
        if !nameField.text!.isEmpty && !emailField.text!.isEmpty {
            submitButton.enabled = true
            submitButton.backgroundColor = darkPink
        } else {
            submitButton.enabled = false
            submitButton.backgroundColor = UIColor.lightGrayColor()
        }
    }
    
    func submit() {
        let appDelegate =
        UIApplication.sharedApplication().delegate as! AppDelegate
        
        let managedContext = appDelegate.managedObjectContext!
        
        //2
        let entity =  NSEntityDescription.entityForName("Member",
            inManagedObjectContext:
            managedContext)
        
        let member = NSManagedObject(entity: entity!,
            insertIntoManagedObjectContext:managedContext)
        
        
        //3
        
        member.setValue(nameField.text, forKey: "name")
        member.setValue(emailField.text, forKey: "email")
        var pn = "None"
        if !pnField.text!.isEmpty {
           pn = pnField.text!
        }
        
        member.setValue(pn, forKey: "pn")
        
        var facebook = true
        
        if facebookCtrl.selectedSegmentIndex == 1 {
            facebook = false
        }
        
        member.setValue(facebook, forKey: "facebook")
        
        //4
        


        do{
            try managedContext.save()
        } catch _ as NSError {
            
        }
        
        data.append(member)
        
        appDelegate.saveContext()

        nameField.text = ""
        emailField.text = ""
        pnField.text = ""
        facebookCtrl.selectedSegmentIndex = 0
    
        //print(data)

    }
    
    
    func askExport(gesture: UILongPressGestureRecognizer) {
        if gesture.state == UIGestureRecognizerState.Began {
            let alert = UIAlertController(title: "Export Data", message: "Would you like to export data?", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.Default, handler: { (UIAlertAction) -> Void in
                self.export()
            }))
            alert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.Cancel, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
        }
    
    }
    
    func export() {
        
        if let dirs : [String] = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentDirectory, NSSearchPathDomainMask.AllDomainsMask, true) {
            let dir = NSURL(fileURLWithPath: dirs[0])

            let path = dir.URLByAppendingPathComponent(("members.csv"));
            var text = "Name,Email,Phone Number,Facebook?"
            for member in data {
                var memberData = [String]()
                
                let name = member.valueForKey("name") as! String
                memberData.append(name)
                
                let email = member.valueForKey("email") as! String
                memberData.append(email)
                
                let pn = member.valueForKey("pn") as! String
                memberData.append(pn)
                
                let facebook = member.valueForKey("facebook") as! Bool
                var fbStr = "No"
                if facebook == true {fbStr = "Yes"}
                memberData.append(fbStr)
                
                let newText = "\n" + memberData.joinWithSeparator(",")
                text += newText
                
            }
            print(text)
    
            do {
                try text.writeToURL(path, atomically: true, encoding: NSUTF8StringEncoding)
                let activity = UIActivityViewController(activityItems: [path], applicationActivities: nil)
                activity.popoverPresentationController!.sourceView = view
                self.presentViewController(activity, animated: true, completion: nil)

            } catch _ as NSError {
                
            }
        

        }
        
        
        
    }
        
    
    func autoFormatTextField() {
        var myTextFieldSemaphore: Int? = nil
        let phoneNumberFormatter = PhoneNumberFormatter()
        let myLocale = "us"
        myTextFieldSemaphore = 0
        if myTextFieldSemaphore  != nil {
            myTextFieldSemaphore = 1
        }
            
        var phoneNumber: String = phoneNumberFormatter.strip(pnField.text)
            
        if(phoneNumber.characters.count > 0) {
            var numOfCharsAllowed = 10;
            if(phoneNumber[phoneNumber.startIndex] == "1") {
                numOfCharsAllowed = 11;
            }
            
            if(phoneNumber.characters.count  > numOfCharsAllowed) {
                phoneNumber = phoneNumber.substringToIndex(phoneNumber.startIndex.advancedBy(numOfCharsAllowed));
            }
        }
        pnField.text = phoneNumberFormatter.format(phoneNumber, withLocale: myLocale)
    
    }
    
    override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {
        for object in self.view.subviews {
            if let o = object as? UITextField {
                if o.isFirstResponder() {
                    o.resignFirstResponder()
                }
            }
        }

    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        if nameField.isFirstResponder() {
            nameField.resignFirstResponder()
            emailField.becomeFirstResponder()
        } else if emailField.isFirstResponder() {
            emailField.resignFirstResponder()
            pnField.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
        }
        return true
    }
    
    func askClear(gesture: UILongPressGestureRecognizer) {
        if gesture.state == UIGestureRecognizerState.Began {
            let alert = UIAlertController(title: "Clear Data", message: "Would you like to clear existing data?", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.Default, handler: { (UIAlertAction) -> Void in
                self.clear()
            }))
            alert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.Cancel, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
        }
        
    }
    
    func clear() {
        let appDelegate =
        UIApplication.sharedApplication().delegate as! AppDelegate
        
        let managedContext = appDelegate.managedObjectContext!
        
        for member in data {
            managedContext.deleteObject(member)
        }
        appDelegate.saveContext()
        data = [NSManagedObject]()
        let alert = UIAlertView(title: "Data Cleared", message: nil, delegate: nil, cancelButtonTitle: "Okay")
        alert.show()
    }


}

